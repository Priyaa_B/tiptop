
div = document.getElementById('leftbar');

document.getElementById("navbar").addEventListener("click", function () {
   div.style.opacity = "0.1";
   unfade(div);
});
document.getElementById("close").addEventListener("click", function () {
   div.style.display = "none";
});

function unfade(element) {
   var op = 0.1; // initial opacity
   element.style.display = 'block';
   var timer = setInterval(function () {
      if (op >= 1) {
         clearInterval(timer);
      }
      element.style.opacity = op;
      element.style.filter = 'alpha(opacity=' + op * 100 + ")";
      op += op * 0.2;
   }, 20);
}