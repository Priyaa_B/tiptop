<?php
require_once(dirname(__DIR__)."/classes/constants.php");
class DBConn
{
    function getdbconnectio(){
        $con = mysqli_connect("localhost",DATABASE_USER,DATABASE_PASS,DATABASE_NAME);
        // Check connection
        if (mysqli_connect_errno()) {
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
            exit();
        }
        return $con;
    }
}