<?php
include("../classes/Queries.php");
$sqlQuery = new Queries();

$members = $sqlQuery->SelectQuery("SELECT * FROM  tiptop_user WHERE `admin`='" . IS_NOT_ADMIN . "' ORDER BY LastName");
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>New Project</title>


	<link rel="stylesheet" type="text/css" href="assets/stylesheets/style.css">

</head>

<body>
<style>
	a.add{
		background: #57419d;
padding: 7px 25px;
color: #fff;
border: none;
font-size: 18px;
border-radius: 5px;
	}
</style>
	<section id="main">

		<?php include('inc/sidebar.php'); ?>

		<div id="rightbar">

			<?php include('inc/header.php'); ?>

			<div id="main_content">
				div
				<a class="add" href="add-campaigns.php">Add Member</a>
				<div class="form_table">
				<select class="form-control" name="state" id="maxRows">
        <option value="5000">Show ALL Rows</option>
        <option value="5">5</option>
        <option value="10">10</option>
        <option value="15">15</option>
        <option value="20">20</option>
        <option value="50">50</option>
        <option value="70">70</option>
        <option value="100">100</option>
      </select>
					<table cellpadding="0" cellspacing="0" border="0" width="100%" id="t01">
					<thead>	
					<tr>
							<th width="10%">Sr. No</th>
							<th width="15%">First Name</th>
							<th width="10%">Last Name</th>
							<th width="20%">Email</th>
							<th width="15%">Action</th>
						</tr>
					</thead>
					<tbody>
						<?php if (count($members) > 0) {
							$inc = 1;
							foreach ($members as $member) {
						?>
								<tr>
									<td><?php echo $inc++; ?></td>
									<td><?php echo $member['FirstName'] ?></td>
									<td><?php echo $member['LastName'] ?></td>
									<td><?php echo $member['email'] ?></td>
									<td>
										<a href="#" class="edit"><img src="assets/images/edit.png"></a>
										<a href="#" class="delete"><img src="assets/images/trash.png"></a>
									</td>
								</tr>
						<?php
							}
						} ?>
						</tbody>
					</table>
					<div class='pagination-container'>
        <nav>
          <ul class="pagination">

            <li data-page="prev">
              <span>
                < <span class="sr-only">
              </span></span>
            </li>
            <!--	Here the JS Function Will Add the Rows -->
            <li data-page="next" id="prev">
              <span> > <span class="sr-only"></span></span>
            </li>
          </ul>
        </nav>
      </div>
				</div>
			</div>
		</div>
	</section>
	<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
	<script src="assets/scripts/custom.js"></script>
	<script>
    getPagination('#t01');
    //getPagination('.table-class');
    //getPagination('table');

    /*					PAGINATION 
    - on change max rows select options fade out all rows gt option value mx = 5
    - append pagination list as per numbers of rows / max rows option (20row/5= 4pages )
    - each pagination li on click -> fade out all tr gt max rows * li num and (5*pagenum 2 = 10 rows)
    - fade out all tr lt max rows * li num - max rows ((5*pagenum 2 = 10) - 5)
    - fade in all tr between (maxRows*PageNum) and (maxRows*pageNum)- MaxRows 
    */


    function getPagination(table) {
      var lastPage = 1;

      $('#maxRows')
        .on('change', function(evt) {
          //$('.paginationprev').html('');						// reset pagination

          lastPage = 1;
          $('.pagination')
            .find('li')
            .slice(1, -1)
            .remove();
          var trnum = 0; // reset tr counter
          var maxRows = parseInt($(this).val()); // get Max Rows from select option

          if (maxRows == 5000) {
            $('.pagination').hide();
          } else {
            $('.pagination').show();
          }

          var totalRows = $(table + ' tbody tr').length; // numbers of rows
          $(table + ' tr:gt(0)').each(function() {
            // each TR in  table and not the header
            trnum++; // Start Counter
            if (trnum > maxRows) {
              // if tr number gt maxRows

              $(this).hide(); // fade it out
            }
            if (trnum <= maxRows) {
              $(this).show();
            } // else fade in Important in case if it ..
          }); //  was fade out to fade it in
          if (totalRows > maxRows) {
            // if tr total rows gt max rows option
            var pagenum = Math.ceil(totalRows / maxRows); // ceil total(rows/maxrows) to get ..
            //	numbers of pages
            for (var i = 1; i <= pagenum;) {
              // for each page append pagination li
              $('.pagination #prev')
                .before(
                  '<li data-page="' +
                  i +
                  '">\
								  <span>' +
                  i++ +
                  '<span class="sr-only"></span></span>\
								</li>'
                )
                .show();
            } // end for i
          } // end if row count > max rows
          $('.pagination [data-page="1"]').addClass('active'); // add active class to the first li
          $('.pagination li').on('click', function(evt) {
            // on click each page
            evt.stopImmediatePropagation();
            evt.preventDefault();
            var pageNum = $(this).attr('data-page'); // get it's number

            var maxRows = parseInt($('#maxRows').val()); // get Max Rows from select option

            if (pageNum == 'prev') {
              if (lastPage == 1) {
                return;
              }
              pageNum = --lastPage;
            }
            if (pageNum == 'next') {
              if (lastPage == $('.pagination li').length - 2) {
                return;
              }
              pageNum = ++lastPage;
            }

            lastPage = pageNum;
            var trIndex = 0; // reset tr counter
            $('.pagination li').removeClass('active'); // remove active class from all li
            $('.pagination [data-page="' + lastPage + '"]').addClass('active'); // add active class to the clicked
            // $(this).addClass('active');					// add active class to the clicked
            limitPagging();
            $(table + ' tr:gt(0)').each(function() {
              // each tr in table not the header
              trIndex++; // tr index counter
              // if tr index gt maxRows*pageNum or lt maxRows*pageNum-maxRows fade if out
              if (
                trIndex > maxRows * pageNum ||
                trIndex <= maxRows * pageNum - maxRows
              ) {
                $(this).hide();
              } else {
                $(this).show();
              } //else fade in
            }); // end of for each tr in table
          }); // end of on click pagination list
          limitPagging();
        })
        .val(5)
        .change();

      // end of on select change

      // END OF PAGINATION
    }

    function limitPagging() {
      // alert($('.pagination li').length)

      if ($('.pagination li').length > 7) {
        if ($('.pagination li.active').attr('data-page') <= 3) {
          $('.pagination li:gt(5)').hide();
          $('.pagination li:lt(5)').show();
          $('.pagination [data-page="next"]').show();
        }
        if ($('.pagination li.active').attr('data-page') > 3) {
          $('.pagination li:gt(0)').hide();
          $('.pagination [data-page="next"]').show();
          for (let i = (parseInt($('.pagination li.active').attr('data-page')) - 2); i <= (parseInt($('.pagination li.active').attr('data-page')) + 2); i++) {
            $('.pagination [data-page="' + i + '"]').show();

          }

        }
      }
    }
  </script>
</body>

</html>