<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<title>New Project</title>


<link rel="stylesheet" type="text/css" href="assets/stylesheets/style.css">

</head>
<body>

<section id="main">
	
	<?php include('inc/sidebar.php'); ?>

	<div id="rightbar">
		
		<?php include('inc/header.php'); ?>

		<div id="main_content">
			<div class="form_inner">
				<p>Fill Campaign Details</p>

				<form>
					<div class="form_main">
						<div class="inner">
							<div class="form-group">
							<label for="firstName">First Name *</label>
                <input type="text" id="first_Name" name="FirstName" placeholder="John" required />
				<p class="error"></p>                
			</div>
							<div class="form-group">
							<label for="lastName">Last Name *</label>
                <input type="text" id="lastName" name="LastName" placeholder="Doe" required />
                <p class="error"></p>
							</div>
							<div class="form-group">
							<label for="e-mail">E-mail address *</label>
                <input type="text" id="email_id" name="email" placeholder="john-doe@net.com" required />
                <p class="error"></p>
							</div>
	                        <div class="form-group">
							<label for="password">Username *</label>
                <input type="text" id="userName" name='userName' required title="6 characters minimum" />
                <p class="error"></p>
						</div>
						<div class="inner">
							<div class="form-group">
								<label> Venue</label>
		                        <textarea class="form-control" name="address" rows="6" required placeholder="Venue"></textarea>
							</div>
							<div class="form-group">
								<label> Description</label>
		                        <textarea class="form-control description" name="description" rows="6" required placeholder="Description"></textarea>
							</div>
						</div>
					</div>
					
					<div class="form-group btn">
						<button type="submit">Save</button>
						<button type="submit" class="cancel">Cancel</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>


<script src="assets/scripts/custom.js"></script>

</body>
</html>