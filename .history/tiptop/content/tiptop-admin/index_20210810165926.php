<?php
include("../../classes/Queries.php");
$sqlQuery = new Queries();

$members = $sqlQuery->SelectQuery("SELECT * FROM  tiptop_user WHERE `admin`='" . IS_NOT_ADMIN . "' ORDER BY LastName");
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>New Project</title>


	<link rel="stylesheet" type="text/css" href="assets/stylesheets/style.css">

</head>

<body>

	<section id="main">

		<?php include('inc/sidebar.php'); ?>

		<div id="rightbar">

			<?php include('inc/header.php'); ?>

			<div id="main_content">
				<div class="form_table">
					<table cellpadding="0" cellspacing="0" border="0" width="100%">
						<tr>
							<th width="10%">Sr. No</th>
							<th width="15%">First Name</th>
							<th width="10%">Image</th>
							<th width="20%">Description</th>
							<th width="15%">Views</th>
							<th width="15%">Date</th>
							<th width="15%">Action</th>
						</tr>
						<tr>
							<td>1</td>
							<td>Lorum ipsum</td>
							<td><img src="assets/images/image.jpg"></td>
							<td>Lorum ipsum dolor</td>
							<td>54</td>
							<td class="date">18/11/2020</td>
							<td>
								<a href="#" class="edit"><img src="assets/images/edit.png"></a>
								<a href="#" class="delete"><img src="assets/images/trash.png"></a>
							</td>
						</tr>
						<?php if (count($members) > 0) {
							$inc = 1;
							foreach ($members as $member) {
						?>
								<tr>
									<td><?php echo $inc++; ?></td>
									<td><?php echo $member['FirstName'] ?></td>
									<td><?php echo $member['LastName'] ?></td>
									<td><?php echo $member['email'] ?></td>
								</tr>
						<?php
							}
						} ?>
					</table>
				</div>
			</div>
		</div>
	</section>

	<script src="assets/scripts/custom.js"></script>

</body>

</html>