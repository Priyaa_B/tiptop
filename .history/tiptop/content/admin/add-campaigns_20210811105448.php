<?php
include("../classes/Queries.php");
$sqlQuery = new Queries();
// 

if (isset($_POST['add_member'])) {
	print_r($_POST);
	exit;
$password = sha1($_POST['password']);
	$add_member = $sqlQuery->InsertQuery("INSERT INTO `tiptop_user`(`userName`, `FirstName`, `LastName`, `password`, `email`, `IDSource`, `RegistrationDate`, `LastLogin`, `admin`) 
	VALUES ('".$_POST['userName']."','".$_POST['FirstName']."','".$_POST['LastName']."','".$password ."','[value-5]','[value-6]','[value-7]','[value-8]','[value-9]','[value-10]')");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>New Project</title>


	<link rel="stylesheet" type="text/css" href="assets/stylesheets/style.css">

</head>

<body>
	<style>
		.error {
			color: red;
			margin-top: -10px;
			margin-bottom: 20px;
			font-size: 14px;
		}
	</style>
	<section id="main">

		<?php include('inc/sidebar.php'); ?>

		<div id="rightbar">

			<?php include('inc/header.php'); ?>

			<div id="main_content">
				<div class="form_inner">
					<p>Fill Campaign Details</p>

					<form method="post" onsubmit="return validateForm()">
						<div class="form_main">
							<div class="inner">
								<div class="form-group">
									<label for="firstName">First Name *</label>
									<input type="text" id="first_Name" name="FirstName" placeholder="John" required />
									<span class="error"></span>
								</div>
								<div class="form-group">
									<label for="lastName">Last Name *</label>
									<input type="text" id="last_Name" name="LastName" placeholder="Doe" required />
									<span class="error"></span>
								</div>
								<div class="form-group">
									<label for="e-mail">E-mail address *</label>
									<input type="text" id="email_id" name="email" placeholder="john-doe@net.com" required />
									<span class="error"></span>
								</div>
								<div class="form-group">
									<label for="e-mail">ID Source *</label>
									<input type="text" id="" name="id_sorce" required ></select
									<span class="error"></span>
								</div>
								<div class="form-group">
									<label for="password">Username *</label>
									<input type="text" id="userName" name='userName' required title="6 characters minimum" />
									<span class="error"></span>
								</div>
								<div class="form-group">
									<label for="password">Password *</label>
									<input type="password" id="password" name="password" pattern=".{6,}" required title="6 characters minimum" />
									<span class="error"></span>
									<span class="password-rules">Your password should contain at least 6 characters and 1 number.</span>
								</div>
								<div class="form-group">
									<style>
										.checkbox input{
											
    width: 4% !important;
    padding: 0px !important;
    border-radius: 0px;
    border: 0px solid rgba(0,0,0,0.3);
}
										
									</style>
									<div class="checkbox">
									<input type="checkbox" id="admin" name="admin" value="<?php echo IS_ADMIN ?>">
									<label for="admin">Is Admin</label><br>
	</div>
								</div>

							</div>

							<div class="form-group btn">
								<button type="submit" name="add_member">Save</button>
								<button type="submit" class="cancel">Cancel</button>
							</div>
					</form>
				</div>
			</div>
		</div>
	</section>

	<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
	<script src="assets/scripts/custom.js"></script>
	<script>
		$(document).on('focusout mouseout', '#email_id', function() {
			var email = $(this).val();
			validateEmail(email);
		}).on('focusout ', '#password', function() {
			var password = $(this).val();
			validatePassword(password);
		}).on('focusout mouseout', '#first_Name', function() {
			var first_Name = $(this).val();
			validateFirstName(first_Name);
		}).on('focusout mouseout', '#last_Name', function() {
			var last_Name = $(this).val();
			validateLastName(last_Name);
		})

		function validateLastName(FirstName) {
			var error_message = document.getElementsByClassName("error");

			if (FirstName == "") {
				text = "";
				error_message[1].innerHTML = text;
				return true;
			} else if (/^[a-zA-Z]+$/.test(FirstName) == true) {
				text = "";
				error_message[1].innerHTML = text;
				return true;
			} else {
				text = "Last name can contain only letters";
				error_message[1].innerHTML = text;
				return false;
			}
		}

		function validateFirstName(FirstName) {
			var error_message = document.getElementsByClassName("error");
			if (FirstName == "") {
				text = "";
				error_message[0].innerHTML = text;
				return true;
			} else if (/^[a-zA-Z]+$/.test(FirstName) == true) {
				text = "";
				error_message[0].innerHTML = text;
				return true;
			} else {
				text = "First name can contain only letters";
				error_message[0].innerHTML = text;
				return false;
			}
		}

		function validateEmail(email_id) {
			var error_message = document.getElementsByClassName("error");
			var format = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

			if (email_id == "" || email_id.match(format)) {
				text = "";
				error_message[2].innerHTML = text;
				return true;
			} else {
				text = "Wrong email format";
				error_message[2].innerHTML = text;
				return false;
			}
		}

		function validatePassword(password) {
			var error_message = document.getElementsByClassName("error");
			var format = /[\w_]/; // allow only letters and numbers
			error_message[4].innerHTML = "";

			if (password == "") {
				text = "";
				error_message[4].innerHTML = text;
				return false;
			} else if (format.test(password) == false) {
				text = "Password contains illegal characters";
				error_message[4].innerHTML = text;
				return false;
			} else if ((password.search(/[0-9]+/) == -1)) {
				text = "Password should contain one or more numbers";
				error_message[4].innerHTML = text;
				return false;
			} else if ((password.length < 6)) {
				text = "Password length should be atleast 6 characters";
				error_message[4].innerHTML = text;
				return false;
			} else {
				text = "";
				error_message[4].innerHTML = text;
				return true;
			}
		}

		function validateForm() {

			var first_name = document.getElementById('first_Name').value;
			var last_name = document.getElementById('last_Name').value;
			var email_id = document.getElementById('email_id').value;
			var password = document.getElementById('password').value;
			if (validateFirstName(first_name) == false) {
				return false;
			}
			if (validateLastName(last_name) == false) {
				return false;
			}
			if (validateEmail(email_id) == false) {
				return false;
			}
			if (validatePassword(password) == false) {
				return false;
			}
			return true;
		}
	</script>
</body>

</html>