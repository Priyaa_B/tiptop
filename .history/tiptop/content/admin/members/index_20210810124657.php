<?php 
include("../../classes/Queries.php");
$sqlQuery = new Queries();

$members = $sqlQuery->SelectQuery("SELECT * FROM  tiptop_user WHERE `admin`='".IS_NOT_ADMIN."' ORDER BY LastName");
// echo "<pre>";
// print_r($members);
// exit;

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <link rel="stylesheet" href="../../../assets/stylesheets/admin-style.css">
</head>

<body>
  <style>
    table {
      width: 100%;
    }

    table,
    th,
    td {
      border: 1px solid black;
      border-collapse: collapse;
    }

    th,
    td {
      padding: 15px;
      text-align: left;
    }

    #t01 tr:nth-child(even) {
      background-color: #eee;
    }

    #t01 tr:nth-child(odd) {
      background-color: #fff;
    }

    #t01 th {
      background-color: black;
      color: white;
    }
    .pagination li:hover{
      color: black;
    cursor: pointer;
}
		table tbody tr {
			display: none;
		}
  </style>
  <header class="page-header">
    <nav>
      <a href="#0" aria-label="forecastr logo" class="logo">
        <svg width="140" height="49">
          <use xlink:href="#logo"></use>
        </svg>
      </a>
      <button class="toggle-mob-menu" aria-expanded="false" aria-label="open menu">
        <svg width="20" height="20" aria-hidden="true">
          <use xlink:href="#down"></use>
        </svg>
      </button>
      <ul class="admin-menu">
        <li class="menu-heading">
          <h3>Admin</h3>
        </li>
        <li>
          <a href="#0">
            <svg>
              <use xlink:href="#pages"></use>
            </svg>
            <span>Pages</span>
          </a>
        </li>
        <li>
          <a href="#0">
            <svg>
              <use xlink:href="#users"></use>
            </svg>
            <span>Users</span>
          </a>
        </li>
        <li>
          <a href="#0">
            <svg>
              <use xlink:href="#trends"></use>
            </svg>
            <span>Trends</span>
          </a>
        </li>
        <li>
          <a href="#0">
            <svg>
              <use xlink:href="#collection"></use>
            </svg>
            <span>Collection</span>
          </a>
        </li>
        <li>
          <a href="#0">
            <svg>
              <use xlink:href="#comments"></use>
            </svg>
            <span>Comments</span>
          </a>
        </li>
        <li>
          <a href="#0">
            <svg>
              <use xlink:href="#appearance"></use>
            </svg>
            <span>Appearance</span>
          </a>
        </li>
        <li class="menu-heading">
          <h3>Settings</h3>
        </li>
        <li>
          <a href="#0">
            <svg>
              <use xlink:href="#settings"></use>
            </svg>
            <span>Settings</span>
          </a>
        </li>
        <li>
          <a href="#0">
            <svg>
              <use xlink:href="#options"></use>
            </svg>
            <span>Options</span>
          </a>
        </li>
        <li>
          <a href="#0">
            <svg>
              <use xlink:href="#charts"></use>
            </svg>
            <span>Charts</span>
          </a>
        </li>
        <li>
          <div class="switch">
            <input type="checkbox" id="mode" checked>
            <label for="mode">
              <span></span>
              <span>Dark</span>
            </label>
          </div>
          <button class="collapse-btn" aria-expanded="true" aria-label="collapse menu">
            <svg aria-hidden="true">
              <use xlink:href="#collapse"></use>
            </svg>
            <span>Collapse</span>
          </button>
        </li>
      </ul>
    </nav>
  </header>
  <section class="page-content">
    <section class="search-and-user">
      <form>
        <input type="search" placeholder="Search Pages...">
        <button type="submit" aria-label="submit form">
          <svg aria-hidden="true">
            <use xlink:href="#search"></use>
          </svg>
        </button>
      </form>
      <div class="admin-profile">
        <span class="greeting">Hello admin</span>
        <div class="notifications">
          <span class="badge">1</span>
          <svg>
            <use xlink:href="#users"></use>
          </svg>
        </div>
      </div>
    </section>
    <section class="grid">
      <h2>Members</h2>
      <select class="form-control" name="state" id="maxRows">
      <option value="5000">Show ALL Rows</option>
						 <option value="5">5</option>
						 <option value="10">10</option>
						 <option value="15">15</option>
						 <option value="20">20</option>
						 <option value="50">50</option>
						 <option value="70">70</option>
						 <option value="100">100</option>
						</select>
      <table id="t01">
        <thead>
        <tr>
          <th>S.No</th>
          <th>Firstname</th>
          <th>Lastname</th>
          <th>Age</th>
        </tr>
        </thead>
        <tbody>
        <?php if(count($members) > 0){
          $inc = 1;
          foreach($members as $member){
            ?>
             <tr>
          <td><?php echo $inc++; ?></td>
          <td>Jill</td>
          <td>Smith</td>
          <td>50</td>
        </tr>
            <?php
          }
        } ?>
              </tbody>
       
      </table>
      <div class='pagination-container' >
				<nav>
				  <ul class="pagination">
            
            <li data-page="prev" >
								     <span> < <span class="sr-only"></span></span>
								    </li>
				   <!--	Here the JS Function Will Add the Rows -->
        <li data-page="next" id="prev">
								       <span> > <span class="sr-only"></span></span>
								    </li>
				  </ul>
				</nav>
			</div>
    </section>
    <footer class="page-footer">
      <span>made by </span>
      <!-- <a href="https://georgemartsoukos.com/" target="_blank">
        <img width="24" height="24" src="https://assets.codepen.io/162656/george-martsoukos-small-logo.svg" alt="George Martsoukos logo">
      </a> -->
    </footer>
  </section>
  <script
			  src="https://code.jquery.com/jquery-3.6.0.min.js"
			  integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
			  crossorigin="anonymous"></script>
  <script>

getPagination('#t01');
					//getPagination('.table-class');
					//getPagination('table');

		  /*					PAGINATION 
		  - on change max rows select options fade out all rows gt option value mx = 5
		  - append pagination list as per numbers of rows / max rows option (20row/5= 4pages )
		  - each pagination li on click -> fade out all tr gt max rows * li num and (5*pagenum 2 = 10 rows)
		  - fade out all tr lt max rows * li num - max rows ((5*pagenum 2 = 10) - 5)
		  - fade in all tr between (maxRows*PageNum) and (maxRows*pageNum)- MaxRows 
		  */
		 

function getPagination(table) {
  var lastPage = 1;

  $('#maxRows')
    .on('change', function(evt) {
      //$('.paginationprev').html('');						// reset pagination

     lastPage = 1;
      $('.pagination')
        .find('li')
        .slice(1, -1)
        .remove();
      var trnum = 0; // reset tr counter
      var maxRows = parseInt($(this).val()); // get Max Rows from select option

      if (maxRows == 5000) {
        $('.pagination').hide();
      } else {
        $('.pagination').show();
      }

      var totalRows = $(table + 'tbody tr').length; // numbers of rows
      $(table + ' tr:gt(0)').each(function() {
        // each TR in  table and not the header
        trnum++; // Start Counter
        if (trnum > maxRows) {
          // if tr number gt maxRows

          $(this).hide(); // fade it out
        }
        if (trnum <= maxRows) {
          $(this).show();
        } // else fade in Important in case if it ..
      }); //  was fade out to fade it in
      if (totalRows > maxRows) {
        // if tr total rows gt max rows option
        var pagenum = Math.ceil(totalRows / maxRows); // ceil total(rows/maxrows) to get ..
        //	numbers of pages
        for (var i = 1; i <= pagenum; ) {
          al
          // for each page append pagination li
          $('.pagination #prev')
            .before(
              '<li data-page="' +
                i +
                '">\
								  <span>' +
                i++ +
                '<span class="sr-only">(current)</span></span>\
								</li>'
            )
            .show();
        } // end for i
      } // end if row count > max rows
      $('.pagination [data-page="1"]').addClass('active'); // add active class to the first li
      $('.pagination li').on('click', function(evt) {
        // on click each page
        evt.stopImmediatePropagation();
        evt.preventDefault();
        var pageNum = $(this).attr('data-page'); // get it's number

        var maxRows = parseInt($('#maxRows').val()); // get Max Rows from select option

        if (pageNum == 'prev') {
          if (lastPage == 1) {
            return;
          }
          pageNum = --lastPage;
        }
        if (pageNum == 'next') {
          if (lastPage == $('.pagination li').length - 2) {
            return;
          }
          pageNum = ++lastPage;
        }

        lastPage = pageNum;
        var trIndex = 0; // reset tr counter
        $('.pagination li').removeClass('active'); // remove active class from all li
        $('.pagination [data-page="' + lastPage + '"]').addClass('active'); // add active class to the clicked
        // $(this).addClass('active');					// add active class to the clicked
	  	limitPagging();
        $(table + ' tr:gt(0)').each(function() {
          // each tr in table not the header
          trIndex++; // tr index counter
          // if tr index gt maxRows*pageNum or lt maxRows*pageNum-maxRows fade if out
          if (
            trIndex > maxRows * pageNum ||
            trIndex <= maxRows * pageNum - maxRows
          ) {
            $(this).hide();
          } else {
            $(this).show();
          } //else fade in
        }); // end of for each tr in table
      }); // end of on click pagination list
	  limitPagging();
    })
    .val(5)
    .change();

  // end of on select change

  // END OF PAGINATION
}

function limitPagging(){
	// alert($('.pagination li').length)

	if($('.pagination li').length > 7 ){
			if( $('.pagination li.active').attr('data-page') <= 3 ){
			$('.pagination li:gt(5)').hide();
			$('.pagination li:lt(5)').show();
			$('.pagination [data-page="next"]').show();
		}if ($('.pagination li.active').attr('data-page') > 3){
			$('.pagination li:gt(0)').hide();
			$('.pagination [data-page="next"]').show();
			for( let i = ( parseInt($('.pagination li.active').attr('data-page'))  -2 )  ; i <= ( parseInt($('.pagination li.active').attr('data-page'))  + 2 ) ; i++ ){
				$('.pagination [data-page="'+i+'"]').show();

			}

		}
	}
}

$(function() {
  // Just to append id number for each row
  $('table tr:eq(0)').prepend('<th> ID </th>');

  var id = 0;

  $('table tr:gt(0)').each(function() {
    id++;
    $(this).prepend('<td>' + id + '</td>');
  });
});

//  Developed By Yasser Mas
// yasser.mas2@gmail.com

  </script>
</body>

</html>