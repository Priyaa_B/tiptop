<?php
include("../../classes/Queries.php");
$sqlQuery = new Queries();

$members = $sqlQuery->SelectQuery("SELECT * FROM  tiptop_user WHERE `admin`='" . IS_NOT_ADMIN . "' ORDER BY LastName");
// echo "<pre>";
// print_r($members);
// exit;

if(isset($_POST['submit'])){

    print_r("test 123 post");
    exit;
    
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../../../assets/stylesheets/admin-style.css">
</head>

<body>
    <style>
        label {
            color: #66cc99;
            text-transform: uppercase;
        }

        input {
            background: transparent;
            font-size: 14px;
            display: block;
            padding: 10px 30px 10px 5px;
            margin: 10px 0 40px;
            border-bottom: 2px solid grey;
        }

        input:focus {
            outline: none;
            border-bottom: 2px solid #66cc99;
            ;
        }

        .radio-question input {
            margin: 10px;
            padding: 0;
        }

        .radio-input {
            display: inline;
        }

        .button {
            background: #66cc99;
            border: 0;
            padding: 15px 30px;
            margin-top: 20px;
            color: white;
            text-transform: uppercase;
            font-size: 13px;
        }

        .button:hover {
            background: #5ec190;
        }

        .password-rules {
            font-size: 10px;
            margin-top: -20px;
            margin-bottom: 40px;
        }

        .error {
            color: red;
            margin-top: -10px;
            margin-bottom: 20px;
            font-size: 14px;
        }

        select,
        textarea,
        input[type="text"],
        input[type="password"],
        input[type="email"] {
            height: 30px;
            width: 100%;
            display: inline-block;
            vertical-align: middle;
            height: 34px;
            padding: 0 10px;
            margin-top: 3px;
            margin-bottom: 10px;
            font-size: 15px;
            line-height: 20px;
            border: 1px solid rgba(255, 255, 255, 0.3);
            background-color: rgba(255, 255, 255, 0.8);
            color: rgba(0, 0, 0, 0.7);
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            border-radius: 2px;
        }
    </style>
    <header class="page-header">
        <nav>
            <a href="#0" aria-label="forecastr logo" class="logo">
                <svg width="140" height="49">
                    <use xlink:href="#logo"></use>
                </svg>
            </a>
            <button class="toggle-mob-menu" aria-expanded="false" aria-label="open menu">
                <svg width="20" height="20" aria-hidden="true">
                    <use xlink:href="#down"></use>
                </svg>
            </button>
            <ul class="admin-menu">
                <li class="menu-heading">
                    <h3>Admin</h3>
                </li>
                <li>
                    <a href="#0">
                        <svg>
                            <use xlink:href="#pages"></use>
                        </svg>
                        <span>Pages</span>
                    </a>
                </li>
                <li>
                    <a href="#0">
                        <svg>
                            <use xlink:href="#users"></use>
                        </svg>
                        <span>Users</span>
                    </a>
                </li>
                <li>
                    <a href="#0">
                        <svg>
                            <use xlink:href="#trends"></use>
                        </svg>
                        <span>Trends</span>
                    </a>
                </li>
                <li>
                    <a href="#0">
                        <svg>
                            <use xlink:href="#collection"></use>
                        </svg>
                        <span>Collection</span>
                    </a>
                </li>
                <li>
                    <a href="#0">
                        <svg>
                            <use xlink:href="#comments"></use>
                        </svg>
                        <span>Comments</span>
                    </a>
                </li>
                <li>
                    <a href="#0">
                        <svg>
                            <use xlink:href="#appearance"></use>
                        </svg>
                        <span>Appearance</span>
                    </a>
                </li>
                <li class="menu-heading">
                    <h3>Settings</h3>
                </li>
                <li>
                    <a href="#0">
                        <svg>
                            <use xlink:href="#settings"></use>
                        </svg>
                        <span>Settings</span>
                    </a>
                </li>
                <li>
                    <a href="#0">
                        <svg>
                            <use xlink:href="#options"></use>
                        </svg>
                        <span>Options</span>
                    </a>
                </li>
                <li>
                    <a href="#0">
                        <svg>
                            <use xlink:href="#charts"></use>
                        </svg>
                        <span>Charts</span>
                    </a>
                </li>
                <li>
                    <div class="switch">
                        <input type="checkbox" id="mode" checked>
                        <label for="mode">
                            <span></span>
                            <span>Dark</span>
                        </label>
                    </div>
                    <button class="collapse-btn" aria-expanded="true" aria-label="collapse menu">
                        <svg aria-hidden="true">
                            <use xlink:href="#collapse"></use>
                        </svg>
                        <span>Collapse</span>
                    </button>
                </li>
            </ul>
        </nav>
    </header>
    <section class="page-content">
        <section class="search-and-user">
            <form>
                <input type="search" placeholder="Search Pages...">
                <button type="submit" aria-label="submit form">
                    <svg aria-hidden="true">
                        <use xlink:href="#search"></use>
                    </svg>
                </button>
            </form>
            <div class="admin-profile">
                <span class="greeting">Hello admin</span>
                <div class="notifications">
                    <span class="badge">1</span>
                    <svg>
                        <use xlink:href="#users"></use>
                    </svg>
                </div>
            </div>
        </section>
        <section class="grid">
            <h2>Add Members</h2>
            <form name="registerForm" method="post" onsubmit="return validateForm()">
                <label for="firstName">First Name *</label>
                <input type="text" id="firstName" name="firstName" placeholder="John" required />
                <p class="error"></p>
                <label for="lastName">Last Name *</label>
                <input type="text" id="lastName" placeholder="Doe" required />
                <p class="error"></p>
                <label for="e-mail">E-mail address *</label>
                <input type="text" id="email_id" placeholder="john-doe@net.com" required />
                <p class="error"></p>
                <label for="password">Password *</label>
                <input type="password" id="password" pattern=".{6,}" required title="6 characters minimum" />
                <p class="error"></p>
                <p class="password-rules">Your password should contain at least 8 characters and 1 number.</p>

                <input class="button" type="submit" value="submit" name="submit"/>
            </form>
        </section>
        <footer class="page-footer">

        </footer>
    </section>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script>
        $(document).on('focusout mouseout', '#email_id', function() {
            var email = $(this).val();
            validateEmail(email);
        }).on('focusout mouseout', '#password', function() {
            var password = $(this).val();
            validatePassword(password);
        })

        function validateFirstName(Frist) {
            var error_message = document.getElementsByClassName("error");
            var format = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

            if (email_id == "" || email_id.match(format)) {
                text = "";
                error_message[2].innerHTML = text;
                return true;
            } else {
                text = "Wrong email format";
                error_message[2].innerHTML = text;
                return false;
            }
        }

        function validateEmail(email_id) {
            var error_message = document.getElementsByClassName("error");
            var format = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

            if (email_id == "" || email_id.match(format)) {
                text = "";
                error_message[2].innerHTML = text;
                return true;
            } else {
                text = "Wrong email format";
                error_message[2].innerHTML = text;
                return false;
            }
        }

        function validatePassword(password) {
            var error_message = document.getElementsByClassName("error");
            var format = /[\w_]/; // allow only letters and numbers
            error_message[3].innerHTML = "";
            if (format.test(password) == false) {
                text = "Password contains illegal characters";
                error_message[3].innerHTML = text;
                return false;
            } else if ((password.search(/[0-9]+/) == -1)) {
                text = "Password should contain one or more numbers";
                error_message[3].innerHTML = text;
                return false;
            } else if ((password.length < 6)) {
                text = "Password length should be atleast 6 characters";
                error_message[3].innerHTML = text;
                return false;
            } else {
                text = "";
                error_message[3].innerHTML = text;
                return true;
            }
        }

        function validateForm() {

            var first_name = document.getElementById('firstName').value;
            var last_name = document.getElementById('lastName').value;
            var email_id = document.getElementById('email_id').value;
            var password = document.getElementById('password').value;
            // if (FirstName(first_name)) {}
            // if (LastName(last_name)) {}
            if (validateEmail(email_id) == false) {
              return false;
            }
            if (validatePassword(password) == false) {
                return false;
            }
            return true;
        }
    </script>
</body>

</html>