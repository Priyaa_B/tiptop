<?php 
include("../../classes/Queries.php");
$sqlQuery = new Queries();

$members = $sqlQuery->SelectQuery("SELECT * FROM  tiptop_user WHERE `admin`='".IS_NOT_ADMIN."' ORDER BY LastName");
// echo "<pre>";
// print_r($members);
// exit;

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <link rel="stylesheet" href="../../../assets/stylesheets/admin-style.css">
</head>

<body>
  <style>
    table {
      width: 100%;
    }

    table,
    th,
    td {
      border: 1px solid black;
      border-collapse: collapse;
    }

    th,
    td {
      padding: 15px;
      text-align: left;
    }

    #t01 tr:nth-child(even) {
      background-color: #eee;
    }

    #t01 tr:nth-child(odd) {
      background-color: #fff;
    }

    #t01 th {
      background-color: black;
      color: white;
    }
    .pagination li:hover{
    cursor: pointer;
}
		table tbody tr {
			display: none;
		}
  </style>
  <header class="page-header">
    <nav>
      <a href="#0" aria-label="forecastr logo" class="logo">
        <svg width="140" height="49">
          <use xlink:href="#logo"></use>
        </svg>
      </a>
      <button class="toggle-mob-menu" aria-expanded="false" aria-label="open menu">
        <svg width="20" height="20" aria-hidden="true">
          <use xlink:href="#down"></use>
        </svg>
      </button>
      <ul class="admin-menu">
        <li class="menu-heading">
          <h3>Admin</h3>
        </li>
        <li>
          <a href="#0">
            <svg>
              <use xlink:href="#pages"></use>
            </svg>
            <span>Pages</span>
          </a>
        </li>
        <li>
          <a href="#0">
            <svg>
              <use xlink:href="#users"></use>
            </svg>
            <span>Users</span>
          </a>
        </li>
        <li>
          <a href="#0">
            <svg>
              <use xlink:href="#trends"></use>
            </svg>
            <span>Trends</span>
          </a>
        </li>
        <li>
          <a href="#0">
            <svg>
              <use xlink:href="#collection"></use>
            </svg>
            <span>Collection</span>
          </a>
        </li>
        <li>
          <a href="#0">
            <svg>
              <use xlink:href="#comments"></use>
            </svg>
            <span>Comments</span>
          </a>
        </li>
        <li>
          <a href="#0">
            <svg>
              <use xlink:href="#appearance"></use>
            </svg>
            <span>Appearance</span>
          </a>
        </li>
        <li class="menu-heading">
          <h3>Settings</h3>
        </li>
        <li>
          <a href="#0">
            <svg>
              <use xlink:href="#settings"></use>
            </svg>
            <span>Settings</span>
          </a>
        </li>
        <li>
          <a href="#0">
            <svg>
              <use xlink:href="#options"></use>
            </svg>
            <span>Options</span>
          </a>
        </li>
        <li>
          <a href="#0">
            <svg>
              <use xlink:href="#charts"></use>
            </svg>
            <span>Charts</span>
          </a>
        </li>
        <li>
          <div class="switch">
            <input type="checkbox" id="mode" checked>
            <label for="mode">
              <span></span>
              <span>Dark</span>
            </label>
          </div>
          <button class="collapse-btn" aria-expanded="true" aria-label="collapse menu">
            <svg aria-hidden="true">
              <use xlink:href="#collapse"></use>
            </svg>
            <span>Collapse</span>
          </button>
        </li>
      </ul>
    </nav>
  </header>
  <section class="page-content">
    <section class="search-and-user">
      <form>
        <input type="search" placeholder="Search Pages...">
        <button type="submit" aria-label="submit form">
          <svg aria-hidden="true">
            <use xlink:href="#search"></use>
          </svg>
        </button>
      </form>
      <div class="admin-profile">
        <span class="greeting">Hello admin</span>
        <div class="notifications">
          <span class="badge">1</span>
          <svg>
            <use xlink:href="#users"></use>
          </svg>
        </div>
      </div>
    </section>
    <section class="grid">
      <h2>Members</h2>
      <select class  ="form-control" name="state" id="maxRows">
      <table id="t01">
        <tr>
          <th>S.No</th>
          <th>Firstname</th>
          <th>Lastname</th>
          <th>Age</th>
        </tr>
        <?php if(count($members) > 0){
          $inc = 1;
          foreach($members as $member){
            ?>
             <tr>
          <td><?php echo $inc++; ?></td>
          <td>Jill</td>
          <td>Smith</td>
          <td>50</td>
        </tr>
            <?php
          }
        } ?>
       
       
      </table>
      <div class='pagination-container' >
				<nav>
				  <ul class="pagination">
            
            <li data-page="prev" >
								     <span> < <span class="sr-only">(current)</span></span>
								    </li>
				   <!--	Here the JS Function Will Add the Rows -->
        <li data-page="next" id="prev">
								       <span> > <span class="sr-only">(current)</span></span>
								    </li>
				  </ul>
				</nav>
			</div>
    </section>
    <footer class="page-footer">
      <span>made by </span>
      <a href="https://georgemartsoukos.com/" target="_blank">
        <img width="24" height="24" src="https://assets.codepen.io/162656/george-martsoukos-small-logo.svg" alt="George Martsoukos logo">
      </a>
    </footer>
  </section>
  <s
</body>

</html>