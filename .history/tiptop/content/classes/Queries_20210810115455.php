<?php
require_once(dirname(__DIR__)."\DB\DBConn.php");
require_once("functions.php");

class SqlQueries extends DBConn
{
    private $mysql;

    public function __construct()
    {
        $this->mysql = $this->getdbconnection();
    }

    public function SelectSingle($mysqlquery)
    {
        $data = array();
        if ($result = $this->mysql->query($mysqlquery)) {
            if ($obj = $result->fetch_assoc()) {
                $data = $obj;
            }
            $result->close();
            return $data;
        } else {
            die("MySQL Error: " . $this->mysql->error);
        }
        return false;
    }

    public function SelectQuery($mysqlquery)
    {
        $data = array();
        if ($result = $this->mysql->query($mysqlquery)) {
            while ($obj = $result->fetch_assoc()) {
                $data[] = $obj;
            }
            $result->close();
            return $data;
        } else {
            die("MySQL Error: " . $this->mysql->error);
        }
        return false;
    }

    public function InsertQuery($mysqlquery)
    {
        if ($this->mysql->query($mysqlquery)) {
            return $this->mysql->insert_id;
        } else {
            die("MySQL Error: " . $this->mysql->error);
        }
    }

    public function UpdateQuery($mysqlquery)
    {
        if (!$this->mysql->query($mysqlquery)) {
            die("MySQL Error: " . $this->mysql->error);
        }else{
            return true;
        }
    }
    public function DeleteQuery($mysqlquery)
    {
        if (!$this->mysql->query($mysqlquery)) {
            die("MySQL Error: " . $this->mysql->error);
        }else{
            return true;
        }
    }

    public function CountQuery($mysqlquery) {
        $count = 0;
        if ($result = $this->mysql->query($mysqlquery)) {
            $count = $result->num_rows;
            $result->close();
            return $count;
        } else {
            die("MySQL Error: " . $this->mysql->error);
        }
        return $count;
    }

}