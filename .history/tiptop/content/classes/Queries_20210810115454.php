<?php
require_once(dirname(__DIR__)."\DB\DBConn.php");
require_once("functions.php");

class SqlQueries extends DBConn
{
    private $mysql;

    public function __construct()
    {
        $this->mysql = $this->getdbconnection();
    }

    public function SelectSingle($mysquery)
    {
        $data = array();
        if ($result = $this->mysql->query($mysquery)) {
            if ($obj = $result->fetch_assoc()) {
                $data = $obj;
            }
            $result->close();
            return $data;
        } else {
            die("MySQL Error: " . $this->mysql->error);
        }
        return false;
    }

    public function SelectQuery($mysquery)
    {
        $data = array();
        if ($result = $this->mysql->query($mysquery)) {
            while ($obj = $result->fetch_assoc()) {
                $data[] = $obj;
            }
            $result->close();
            return $data;
        } else {
            die("MySQL Error: " . $this->mysql->error);
        }
        return false;
    }

    public function InsertQuery($mysquery)
    {
        if ($this->mysql->query($mysquery)) {
            return $this->mysql->insert_id;
        } else {
            die("MySQL Error: " . $this->mysql->error);
        }
    }

    public function UpdateQuery($mysquery)
    {
        if (!$this->mysql->query($mysquery)) {
            die("MySQL Error: " . $this->mysql->error);
        }else{
            return true;
        }
    }
    public function DeleteQuery($mysquery)
    {
        if (!$this->mysql->query($mysquery)) {
            die("MySQL Error: " . $this->mysql->error);
        }else{
            return true;
        }
    }

    public function CountQuery($mysquery) {
        $count = 0;
        if ($result = $this->mysql->query($mysquery)) {
            $count = $result->num_rows;
            $result->close();
            return $count;
        } else {
            die("MySQL Error: " . $this->mysql->error);
        }
        return $count;
    }

}