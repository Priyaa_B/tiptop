<?php
require_once(dirname(__DIR__) . "\DB\DBConn.php");
require_once("functions.php");

class Queries extends DBConn
{
    private $mysql;

    public function __construct()
    {
        $this->mysql = $this->getdbconnection();
    }

    public function SelectSingle($mysqlQuery)
    {
        $data = array();
        if ($result = $this->mysql->query($mysqlQuery)) {
            if ($obj = $result->fetch_assoc()) {
                $data = $obj;
            }
            $result->close();
            return $data;
        } else {
            die("MySQL Error: " . $this->mysql->error);
        }
        return false;
    }

    public function SelectQuery($mysqlQuery, $letters,$values)
    {
        $data = array();
        $stmt = $this->mysql->prepare($mysqlQuery);
        $stmt->bind_param($letters, $values);
  
        if ( $stmt->execute()) {
            $result = $stmt->get_result();
            $data = $result->fetch_assoc();
            whi
            return $data;
        } else {
            die("MySQL Error: " . $this->mysql->error);
        }
        return false;
    }
    // public function SelectQuery($mysqlQuery)
    // {
    //     $data = array();
    //     if ($result = $this->mysql->query($mysqlQuery)) {
    //         while ($obj = $result->fetch_assoc()) {
    //             $data[] = $obj;
    //         }
    //         $result->close();
    //         return $data;
    //     } else {
    //         die("MySQL Error: " . $this->mysql->error);
    //     }
    //     return false;
    // }

    public function InsertQuery($mysqlQuery, $letters, $POST)
    {
        $stmt = $this->mysql->prepare($mysqlQuery);
        $stmt->bind_param($letters, $POST);

        if ($stmt->execute()) {
            return true;
        } else {
            die("MySQL Error: " . $this->mysql->error);
        }
    }
    // public function InsertQuery($mysqlQuery)
    // {
    //     if ($this->mysql->query($mysqlQuery)) {
    //         return $this->mysql->insert_id;
    //     } else {
    //         die("MySQL Error: " . $this->mysql->error);
    //     }
    // }

    public function UpdateQuery($mysqlQuery)
    {
        if (!$this->mysql->query($mysqlQuery)) {
            die("MySQL Error: " . $this->mysql->error);
        } else {
            return true;
        }
    }
    public function DeleteQuery($mysqlQuery)
    {
        if (!$this->mysql->query($mysqlQuery)) {
            die("MySQL Error: " . $this->mysql->error);
        } else {
            return true;
        }
    }

    public function CountQuery($mysqlQuery)
    {
        $count = 0;
        if ($result = $this->mysql->query($mysqlQuery)) {
            $count = $result->num_rows;
            $result->close();
            return $count;
        } else {
            die("MySQL Error: " . $this->mysql->error);
        }
        return $count;
    }
}
