<?php
require_once(dirname(__DIR__)."\DB\DBConn.php");
require_once("functions.php");

class SqlQueries extends DBConn
{
    private $mysql;

    public function __construct()
    {
        $this->mysql = $this->getdbconnection();
    }

    public function SelectSingle($query)
    {
        $data = array();
        if ($result = $this->mysql->query($query)) {
            if ($obj = $result->fetch_assoc()) {
                $data = $obj;
            }
            $result->close();
            return $data;
        } else {
            die("MySQL Error: " . $this->mysql->error);
        }
        return false;
    }

    public function SelectQuery($query)
    {
        $data = array();
        if ($result = $this->mysql->query($query)) {
            while ($obj = $result->fetch_assoc()) {
                $data[] = $obj;
            }
            $result->close();
            return $data;
        } else {
            die("MySQL Error: " . $this->mysql->error);
        }
        return false;
    }

    public function InsertQuery($query)
    {
        if ($this->mysql->query($query)) {
            return $this->mysql->insert_id;
        } else {
            die("MySQL Error: " . $this->mysql->error);
        }
    }

    public function UpdateQuery($query)
    {
        if (!$this->mysql->query($query)) {
            die("MySQL Error: " . $this->mysql->error);
        }else{
            return true;
        }
    }
    public function DeleteQuery($query)
    {
        if (!$this->mysql->query($query)) {
            die("MySQL Error: " . $this->mysql->error);
        }else{
            return true;
        }
    }

    public function CountQuery($query) {
        $count = 0;
        if ($result = $this->mysql->query($query)) {
            $count = $result->num_rows;
            $result->close();
            return $count;
        } else {
            die("MySQL Error: " . $this->mysql->error);
        }
        return $count;
    }

}