<?php
require_once(dirname(__DIR__)."\DB\DBConn.php");
require_once("functions.php");

class SqlQueries extends DBConn
{
    private $mysql;

    public function __construct()
    {
        $this->mysql = $this->getdbconnection();
    }

    public function SelectSingle($mysql@uery)
    {
        $data = array();
        if ($result = $this->mysql->query($mysql@uery)) {
            if ($obj = $result->fetch_assoc()) {
                $data = $obj;
            }
            $result->close();
            return $data;
        } else {
            die("MySQL Error: " . $this->mysql->error);
        }
        return false;
    }

    public function SelectQuery($mysql@uery)
    {
        $data = array();
        if ($result = $this->mysql->query($mysql@uery)) {
            while ($obj = $result->fetch_assoc()) {
                $data[] = $obj;
            }
            $result->close();
            return $data;
        } else {
            die("MySQL Error: " . $this->mysql->error);
        }
        return false;
    }

    public function InsertQuery($mysql@uery)
    {
        if ($this->mysql->query($mysql@uery)) {
            return $this->mysql->insert_id;
        } else {
            die("MySQL Error: " . $this->mysql->error);
        }
    }

    public function UpdateQuery($mysql@uery)
    {
        if (!$this->mysql->query($mysql@uery)) {
            die("MySQL Error: " . $this->mysql->error);
        }else{
            return true;
        }
    }
    public function DeleteQuery($mysql@uery)
    {
        if (!$this->mysql->query($mysql@uery)) {
            die("MySQL Error: " . $this->mysql->error);
        }else{
            return true;
        }
    }

    public function CountQuery($mysql@uery) {
        $count = 0;
        if ($result = $this->mysql->query($mysql@uery)) {
            $count = $result->num_rows;
            $result->close();
            return $count;
        } else {
            die("MySQL Error: " . $this->mysql->error);
        }
        return $count;
    }

}