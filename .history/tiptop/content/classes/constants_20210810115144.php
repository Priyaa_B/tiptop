<?php

define("BASE_PATH", "/KF7013/tiptop");
define("MAIN_APPLICATION_PATH", $_SERVER['DOCUMENT_ROOT'] . BASE_PATH);
define("UPLOAD_PATH_ORG", MAIN_APPLICATION_PATH . "/uploads/");
define("FRONTEND_UPLOAD_PATH_ORG", BASE_PATH . "/uploads/");
define("DATABASE_USER", "root");
define("DATABASE_PASS", "");
define("DATABASE_NAME", "tiptop");
define('IS_ADM','2');

//stock buy/sell action
define('ACTION_BUY','1');
define('ACTION_SELL','2');
define('BUY','buy');
define('SELL','sell');

//Wallet History
define('TYPE_WITHDRAWN','1');
define('TYPE_DEPOSITED','2');
define('WITHDRAWN','withdrawn');
define('DEPOSITED','deposited');
$history_type = array('1' =>'Withdrawn', '2' => 'Deposited');

//Wallet action text
define('WITHDRAWN_TEXT','Bought Share -');
define('DEPOSIT_TEXT','Amount added to wallet');
define('STOCK_SELL_DEPOSITED_TEXT','Share sold -');